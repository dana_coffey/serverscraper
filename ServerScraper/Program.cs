﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.DirectoryServices;
using System.Runtime.CompilerServices;
using System.Data.SqlClient;
using System.Management;

using System.Net;

namespace ServerScraper
{
    class Program
    {
        private static WmiManager wm = new WmiManager();
        private static DataManager dm = new DataManager();
        private static Utilities utils = new Utilities();


        public static void Main(string[] args)
        {
            List<Server> serverList = new List<Server>();
            Server newServer = new Server();
            DirectoryEntry dirEntry = new DirectoryEntry("LDAP://DC=mingledorffs,DC=com");
            DirectorySearcher dirSearch = new DirectorySearcher(dirEntry);
            Type myType = typeof(Server);
            PropertyInfo[] myProperties = myType.GetProperties();

            dirSearch.PropertiesToLoad.Add("name");
            dirSearch.PropertiesToLoad.Add("cn");
            dirSearch.PropertiesToLoad.Add("dnshostname");
            dirSearch.PropertiesToLoad.Add("operatingsystem");
            dirSearch.PropertiesToLoad.Add("operatingsystemversion");
            dirSearch.PropertiesToLoad.Add("operatingsystemservicepack");
            dirSearch.PropertiesToLoad.Add("memberof");
            dirSearch.PropertiesToLoad.Add("operatingsystem");
            dirSearch.PropertiesToLoad.Add("whencreated");
            dirSearch.PropertiesToLoad.Add("whenchanged");
            dirSearch.PropertiesToLoad.Add("lastlogontimestamp");
            dirSearch.PropertiesToLoad.Add("distinguishedname");
            dirSearch.Filter = "(OperatingSystem=Windows*Server*)";

            var results = dirSearch.FindAll();
            foreach (SearchResult sResult in results)
            {
                newServer = new Server();

                string searchpath = sResult.Path;

                if (!searchpath.ToLower().Contains("ou=disabled") && searchpath.ToLower().Contains("ou=servers"))
                {
                    ResultPropertyCollection rpc = sResult.Properties;
                    foreach (string property in rpc.PropertyNames)
                    {
                        foreach (object value in rpc[property])
                            if (property.ToString() == "name" ||
                                property.ToString() == "cn" ||
                                property.ToString() == "dnshostname" ||
                                property.ToString() == "operatingsystem" ||
                                property.ToString() == "operatingsystemversion" ||
                                property.ToString() == "operatingsystemservicepack" ||
                                property.ToString() == "memberof" ||
                                property.ToString() == "whenchanged" ||
                                property.ToString() == "whencreated" ||
                                property.ToString() == "distinguishedname" ||
                                property.ToString() == "lastlogontimestamp")

                            {
                                if (property.ToString() == "name")
                                {
                                    newServer.name = value.ToString();
                                    if (newServer.ou != "disabled")
                                    {
                                        newServer.ipAddress = utils.NSLookup(value.ToString())[0];
                                    }
                                }

                                if (property.ToString() == "cn") newServer.cn = value.ToString();
                                if (property.ToString() == "dnshostname") newServer.dnshostname = value.ToString();
                                if (property.ToString() == "distinguishedname") newServer.distinguishedname = value.ToString();
                                if (property.ToString() == "operatingsystem") newServer.operatingsystem = value.ToString();
                                if (property.ToString() == "operatingsystemversion")
                                    newServer.operatingsystemversion = value.ToString();
                                if (property.ToString() == "operatingsystemservicepack")
                                    newServer.operatingsystemservicepack = value.ToString();
                                if (property.ToString() == "memberof") newServer.memberof = value.ToString();
                                if (property.ToString() == "whencreated")
                                    newServer.created = value.ToString();
                                if (property.ToString() == "whenchanged")
                                    newServer.modified = value.ToString();
                                if (property.ToString() == "lastlogontimestamp")
                                {
                                    newServer.lastlogondate = value.ToString();
                                }
                            }
                    }

                    // OU Group
                    string[] ouArray = newServer.distinguishedname.Split(",".ToCharArray());
                    string holder = " | ";
                    foreach (string ou in ouArray)
                    {
                        if (ou.Contains("OU="))
                        {
                            holder += ou.Replace("OU=", "") + " | ";
                        }
                    }
                    newServer.ou = holder.Trim();

                    // RAM - Memory
                    newServer.capacity = wm.GetRam(newServer.name);

                    // disk info
                    newServer.diskDrives = wm.GetDiskInfo(newServer.name);
                    serverList.Add(newServer);
                }

               
            }

            dm.InsertServers(serverList);
        }
    }
}

