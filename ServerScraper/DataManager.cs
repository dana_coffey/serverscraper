﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;

namespace ServerScraper
{
    class DataManager
    {
        private string strconn = System.Configuration.ConfigurationManager.ConnectionStrings["connString"].ToString();
        private WmiManager wm = new WmiManager();


        public void InsertServers(List<Server> loadedServers)
        {
            foreach (var srv in loadedServers)
            {
                try
                {
                    using (SqlConnection conn = new SqlConnection(strconn))
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand("AddServers", conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("name", SqlDbType.VarChar).Value = srv.name;
                            cmd.Parameters.Add("cn", SqlDbType.VarChar).Value = srv.cn;
                            cmd.Parameters.Add("distinguishedname", SqlDbType.VarChar).Value = srv.distinguishedname;
                            cmd.Parameters.Add("dnshostname", SqlDbType.VarChar).Value = srv.dnshostname;
                            cmd.Parameters.Add("description", SqlDbType.VarChar).Value = srv.description;
                            cmd.Parameters.Add("operatingsystem", SqlDbType.VarChar).Value = srv.operatingsystem;
                            cmd.Parameters.Add("operatingsystemversion", SqlDbType.VarChar).Value =
                                srv.operatingsystemversion;
                            cmd.Parameters.Add("operatingsystemservicepack", SqlDbType.VarChar).Value =
                                srv.operatingsystemservicepack;
                            cmd.Parameters.Add("ipaddress", SqlDbType.VarChar).Value = srv.ipAddress;
                            cmd.Parameters.Add("ougroup", SqlDbType.VarChar).Value = srv.ou;
                            cmd.Parameters.Add("memberof", SqlDbType.VarChar).Value = srv.memberof;
                            cmd.Parameters.Add("createdate", SqlDbType.VarChar).Value = srv.created;
                            cmd.Parameters.Add("modifieddate", SqlDbType.VarChar).Value = srv.modified;
                            cmd.Parameters.Add("lastlogondate", SqlDbType.VarChar).Value = srv.lastlogondate;
                            cmd.Parameters.Add("capacity", SqlDbType.VarChar).Value = srv.capacity;
                            cmd.Parameters.Add("IIS", SqlDbType.VarChar).Value = " - ";  //srv.IIS;

                            cmd.ExecuteNonQuery();
                        }
                        conn.Close();
                        conn.Dispose();
                    }

                    InsertDiskDrives(srv.diskDrives);
                }
                catch (Exception ex)
                {
                    // handle later
                }
            }


        }

        public void InsertDiskDrives(List<ServerDrive> drives)
        {

            foreach (var dr in drives)
            {
                try
                {
                    using (SqlConnection conn = new SqlConnection(strconn))
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand("AddDrives", conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("serverName", SqlDbType.VarChar).Value = dr.serverName;
                            cmd.Parameters.Add("driveLetter", SqlDbType.VarChar).Value = dr.driveLetter;
                            cmd.Parameters.Add("freespaceingb", SqlDbType.Decimal).Value = dr.freespaceInGb;
                            cmd.Parameters.Add("sizeingb", SqlDbType.Decimal).Value = dr.sizeInGb;
                            cmd.Parameters.Add("freespacepercent", SqlDbType.VarChar).Value = dr.freespacePercent;
                            cmd.Parameters.Add("filesystem", SqlDbType.VarChar).Value = dr.filesystem;
                            cmd.ExecuteNonQuery();
                        }

                        conn.Close();
                        conn.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    // handle later
                }
            }
        }
    }
}
