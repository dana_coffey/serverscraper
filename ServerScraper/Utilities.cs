﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ServerScraper
{
    class Utilities
    {
        public  List<string> NSLookup(string arg1)
        {
            List<string> foundips = new List<string>();
            try
            {
                IPHostEntry ipEntry;
                IPAddress[] ipAddr;
                char[] alpha = "aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ-".ToCharArray();

                //If alpha characters exist we know we are doing a forward lookup
                if (arg1.IndexOfAny(alpha) != -1)
                {
                    ipEntry = Dns.GetHostEntry(arg1);
                    ipAddr = ipEntry.AddressList;

                    int i = 0;
                    int len = ipAddr.Length;

                    for (i = 0; i < len; i++)
                    {
                        foundips.Add(ipAddr[i].ToString());
                    }
                }
            }
            catch (Exception fe)
            {
                //handle later
                //Console.WriteLine(fe.Message.ToString());
            }

            return foundips;
        }
    }
}
