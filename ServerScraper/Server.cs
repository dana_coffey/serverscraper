﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerScraper
{
    class Server
    {
        public string name { get; set; }
        public string cn { get; set; }

        public string distinguishedname { get; set; }

        public string dnshostname { get; set; }

        public string operatingsystem { get; set; }
        public string description { get; set; }
        public string ipv4address { get; set; }
        public string ipv6address { get; set; }
        public string operatingsystemversion { get; set; }
        public string operatingsystemservicepack { get; set; }
        public string memberof { get; set; }
        public string created { get; set; }

        public string modified { get; set; }

        public string lastlogondate { get; set; }

        public string ipAddress { get; set; }

        public string ou { get; set; }
        public string capacity { get; set; }

        public List<ServerDrive> diskDrives { get; set; }

        public string IIS { get; set; }
    }
}
