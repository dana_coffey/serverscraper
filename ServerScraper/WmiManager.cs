﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Management;

using System.Net.Sockets;
using Microsoft.Management.Infrastructure;
using Microsoft.Management.Infrastructure.Options;
using System.Security;

namespace ServerScraper
{
    public class WmiManager
    {
        public string GetRam(string serverName)
        {
            try
            {
                ConnectionOptions options = new ConnectionOptions();
                options.Impersonation = System.Management.ImpersonationLevel.Impersonate;


                ManagementScope scope = new ManagementScope("\\\\" + serverName + "\\root\\cimv2", options);
                scope.Connect();

                ////Query system for Operating System information
                //ObjectQuery query = new ObjectQuery("SELECT * FROM Win32_OperatingSystem");
                //ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, query);

                //ManagementObjectCollection queryCollection = searcher.Get();
                //foreach (ManagementObject m in queryCollection)
                //{
                //    // Display the remote computer information
                //    Console.WriteLine("Computer Name     : {0}", m["csname"]);
                //    Console.WriteLine("Windows Directory : {0}", m["WindowsDirectory"]);
                //    Console.WriteLine("Operating System  : {0}", m["Caption"]);
                //    Console.WriteLine("Version           : {0}", m["Version"]);
                //    Console.WriteLine("Manufacturer      : {0}", m["Manufacturer"]);
                //}

                ObjectQuery query = new ObjectQuery("SELECT capacity FROM Win32_PhysicalMemory");
                ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, query);
                ManagementObjectCollection queryCollection = searcher.Get();
                UInt64 capacity = 0;
                foreach (ManagementObject m in queryCollection)
                {
                    capacity += (UInt64)m["Capacity"];

                }

                ulong v = capacity / (1024 * 1024 * 1024);
                double x = v;
                return x.ToString();
            }
            catch (Exception ee)
            {
                return "0";
            }

        }

        public List<ServerDrive> GetDiskInfo(string serverName)
        {
            string retValue = string.Empty;
            List<ServerDrive> serverDrives = new List<ServerDrive>();
            ServerDrive sd = new ServerDrive();

            try
            {
                ConnectionOptions options = new ConnectionOptions();
                options.Impersonation = System.Management.ImpersonationLevel.Impersonate;


                ManagementScope scope = new ManagementScope("\\\\" + serverName + "\\root\\cimv2", options);
                scope.Connect();


                ObjectQuery query = new ObjectQuery("SELECT Size, FreeSpace, Name, FileSystem FROM Win32_LogicalDisk WHERE DriveType = 3");
                ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, query);
                ManagementObjectCollection queryCollection = searcher.Get();

                

                foreach (ManagementObject m in queryCollection)
                {
                   
                    decimal size = Convert.ToDecimal(m["Size"]) / 1024 / 1024 / 1024;
                    decimal freeSpace = Convert.ToDecimal(m["FreeSpace"]) / 1024 / 1024 / 1024;
                    //Console.WriteLine("Server: " + serverName);
                    //Console.WriteLine("Drive: " + m["Name"].ToString());
                    //Console.WriteLine("Size: " + (Decimal.Round(size, 2)).ToString() + " GB");
                    //Console.WriteLine("Free Space: " + (Decimal.Round(freeSpace, 2)).ToString() + " GB");
                    //Console.WriteLine("Percent Free: " +(Decimal.Round(freeSpace / size, 2) * 100).ToString() + @"%");

                    sd = new ServerDrive();
                    sd.serverName = serverName;
                    sd.driveLetter = m["Name"].ToString();
                    sd.filesystem = m["FileSystem"].ToString();
                    sd.freespaceInGb = Decimal.Round(freeSpace,2);
                    sd.sizeInGb = Decimal.Round(size, 2);
                    sd.freespacePercent = (Decimal.Round(freeSpace / size, 2) * 100).ToString();


                    serverDrives.Add(sd);

                }

            }
            catch (Exception ee)
            {
                //
            }


            return serverDrives;

        }



    }
}
