﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerScraper
{
    public class ServerDrive
    {
        public string serverName { get; set; }
        public string driveLetter { get; set; }
        public decimal sizeInGb { get; set; }
        public decimal freespaceInGb { get; set; }
        public string freespacePercent { get; set; }
        public string filesystem { get; set; }
    }
}
